# Official Python Docker Image https://hub.docker.com/_/python
FROM python:3.7-slim-buster

# Copy the /src folder to the container
ADD /src /src

# Install python dependencies
WORKDIR /src
RUN pip install --no-cache-dir -r /src/requirements.txt