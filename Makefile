## Variables
image_name = python:3.7-slim-buster
container_name = udemy_complete_python_course_container

## Container Provision
proxy:
	@chmod 777 ./proxy.sh && ./proxy.sh

build:
	docker-compose build --pull --no-cache

run:
	docker-compose up -d

connect:
	docker exec -it $(container_name) bash

## Container Cleanup
clean: stop delete-containers delete-images

stop:
	docker-compose down --remove-orphans

delete-containers:
	docker system prune -f

delete-images:
	docker rmi $$(docker images -a -q) -f

## Stages
dev: proxy stop build run connect

## Application
venv:
	cd /src && \
	virtualenv -p python3 venv && \
	. venv/bin/activate && \
	pip install --no-cache-dir -r /src/requirements.txt