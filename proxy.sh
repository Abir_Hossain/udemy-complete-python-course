#!/bin/bash
 
# Set proxy if host is currently within SBS Office
SBS_DNS=10.121.32.81
SBS_PROXY_ADDRESS=http://sydproxy.unix.sbs.com.au:8080
ENV_FILE=./.env
 
case "$OSTYPE" in
    msys*) OS=windows ;;
    darwin*) OS=mac ;;
    linux*) OS=linux ;;
    *) ;; # Default is null
esac
 
case $OS in
    windows) ping -n 1 -w 1 $SBS_DNS > /dev/null ;;
    mac|linux) ping -c 1 $SBS_DNS > /dev/null ;;
    *) ;;
esac
 
 
# Check if user is currently on SBS network
if [ $? -eq 0 ]; then
    echo "*** Auto-Proxy Detection Setting: TRUE - You are currently on the SBS Network ***"
    if  ! grep -qi "http_proxy" $ENV_FILE; then     # If proxy environment variables are not already in the dev env file, set them
        echo "" >> $ENV_FILE
        echo "http_proxy=$SBS_PROXY_ADDRESS" >> $ENV_FILE
        echo "https_proxy=$SBS_PROXY_ADDRESS" >> $ENV_FILE
        echo "HTTP_PROXY=$SBS_PROXY_ADDRESS" >> $ENV_FILE
        echo "HTTPS_PROXY=$SBS_PROXY_ADDRESS" >> $ENV_FILE
    fi
else     # If user is not on SBS network
    echo "*** Auto-Proxy Detection Setting: FALSE - You are currently NOT on the SBS Network ***"
 
    if  grep -qi "http_proxy" $ENV_FILE; then    # If proxy environment variables are currently stored, remove them
 
 
        case $OS in
            windows) 
                sed -i "/http_proxy/I d" $ENV_FILE
                sed -i "/https_proxy/I d" $ENV_FILE
                ;;
            mac|linux)
                sed -i '' '/http_proxy/d' $ENV_FILE
                sed -i '' '/https_proxy/d' $ENV_FILE
                sed -i '' '/HTTP_PROXY/d' $ENV_FILE
                sed -i '' '/HTTPS_PROXY/d' $ENV_FILE
                ;;
            *) ;;
        esac
    fi
fi