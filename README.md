## What is this repository for?

This is my personal repo for saving my progress on the [Udemy Complete Python Course](https://www.udemy.com/course/the-complete-python-course/).

I'm using docker for my local python setup to keep my dev machine clean and make it easier to continue the course from any machine in the office or at home. The image used is one of the official [python docker images](https://hub.docker.com/_/python) available from Dockerhub.

### Try it yourself

```bash
# Clone the repository
git clone git@bitbucket.org:Abir_Hossain/udemy-complete-python-course.git

# Change directory to the repository
cd /udemy-complete-python-course

# Get your development environment up and running.
make dev

You will automatically be taken into a SSH shell within the container after it starts.
You can now make changes within the /src folder in realtime.
```

### Using an IDE

Open the `udemy-complete-python-course` folder within your IDE.

The `src` folder is a mounted volume within the container. Files created/edited within this folder from either the host machine/container will be reflected in both realtime.